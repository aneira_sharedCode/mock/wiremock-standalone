# MockServer 
 
Tool : WireMok

Link : http://wiremock.org/docs/running-standalone/

## Pre-requirement

1. Java 8 or +
2. Set Mapping Request and Response 

## Clone

1 . Clone the Project

    With ssh  : git@gitlab.com:aneira_sharedCode/wiremock-standalone.git
        
    With https : https://gitlab.com/aneira_sharedCode/wiremock-standalone.git

## Run

Execute start bash file.
    
    ./bin/start.sh

## Configuration

1 . The port and root directory settings are located in the start.sh file.

    Parameters:
    
        --port          -> Set the HTTP port number e.g. --port 9999. Use --port 0 to dynamically determine a port.
        --root-dir      -> Sets the root directory, under which mappings and __files reside. This defaults to the current directory.

   Example:
    
        java -jar ${PATH}/deployment/wiremock-standalone-2.27.0.jar --port 9991 --root-dir ${PATH}/src


2 . Una vez añadidos los argumentos, ejecutar la aplicación.

    Linea de Comando o SSH file

        nohup java -jar ${SCRIPT_PATH}/deployment/wiremock-standalone-2.27.0.jar --port 9991 --root-dir ${SCRIPT_PATH}/src &

    IDE (Eclipse o Intellij Idea)
    
        Run configuration
    


