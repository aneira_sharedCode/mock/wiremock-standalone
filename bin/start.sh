#!/bin/bash

#SCRIPT_PATH=`pwd`;
WM_PATH="/Users/andy.neira/Workspace/LEARN/wiremock-standalone"
WM_PORT=9991
echo ==========================================
echo Start WireMock 2
echo Path : ${WM_PATH}
echo Port : ${WM_PORT}
nohup java -jar ${WM_PATH}/deployment/wiremock-standalone-2.27.0.jar --port ${WM_PORT} --root-dir ${WM_PATH}/src > ${WM_PATH}/log/application.log 2>&1 &
echo MockServer is running ...
echo ==========================================